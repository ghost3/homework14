#include <iostream>
#include <string>


int main()
{
    std::string MyString = "This is string";

    std::cout << "Whole string: " << MyString << '\n';
    std::cout << "String length: " << MyString.length() << '\n';
    std::cout << "First symbol: " << MyString[0] << '\n';
    std::cout << "Last symbol: " << MyString[MyString.length() - 1] << '\n';
}